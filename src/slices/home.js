import { createSlice } from '@reduxjs/toolkit'
import { addDays } from 'date-fns';

const defaultDateRange = [
	{
		startDate: new Date(),
		endDate: addDays(new Date(), 1),
		key: 'selection'
	}
];

const getDisplaySelectedDateRange = (selectedDateRange) => {
	let displaySelectedDateRange = '';
	try {
		displaySelectedDateRange = `${selectedDateRange[0].startDate.toLocaleDateString('en-US')}- ${selectedDateRange[0].endDate.toLocaleDateString('en-US')}`
	} catch (error) { }
	return displaySelectedDateRange;
}

const reArrangeSelectedReports = (selectedReports) => {
	// const lastItem = selectedReports[selectedReports.length - 1];
	// const firsItem = selectedReports[0];
	// selectedReports[selectedReports.length - 1] = firsItem;
	// selectedReports[0] = lastItem;
	return selectedReports.filter(item => item !== undefined).reverse();
}

const homeSlice = createSlice({
	name: 'home',
	initialState: {
		selectedReports: [],
		selectedDateRange: defaultDateRange,
		displaySelectedDateRange: getDisplaySelectedDateRange(defaultDateRange),
	},
	reducers: {
		updateSelectedReports: (state, action) => {
			state.selectedReports = reArrangeSelectedReports(action.payload);
		},
		updateSelectedDateRange: (state, action) => {
			state.selectedDateRange = action.payload;
			state.displaySelectedDateRange = getDisplaySelectedDateRange(action.payload)
		},
	},
});

export const { updateSelectedReports, updateSelectedDateRange } = homeSlice.actions;
export const selectedReportsSelector = (state) => state.home.selectedReports;
export const selectedDateRangeSelector = (state) => state.home.selectedDateRange;
export const displaySelectedDateRangeSelector = (state) => state.home.displaySelectedDateRange;

export default homeSlice.reducer;