import * as React from 'react';
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { mainListItems } from './listItems';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import ClientsAutoComplete from './ClientsAutoComplete';
import ReportsAutoComplete from './ReportsAutoComplete';
import BasicDateRangePicker from './BasicDateRangePicker';
import TopCard from './topCard/TopCard';
import ChartWrapper from './charts/ChatWrapper';
import { useDispatch, useSelector } from "react-redux";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import { selectedReportsSelector, displaySelectedDateRangeSelector, selectedDateRangeSelector } from '../slices/home';
import { updateSelectedDateRange } from '../slices/home';

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
	zIndex: theme.zIndex.drawer + 1,
	transition: theme.transitions.create(['width', 'margin'], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	}),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
	({ theme, open }) => ({
		'& .MuiDrawer-paper': {
			position: 'relative',
			whiteSpace: 'nowrap',
			width: drawerWidth,
			transition: theme.transitions.create('width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
			boxSizing: 'border-box',
			...(!open && {
				overflowX: 'hidden',
				transition: theme.transitions.create('width', {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
				}),
				width: theme.spacing(7),
				[theme.breakpoints.up('sm')]: {
					width: theme.spacing(9),
				},
			}),
		},
	}),
);

const mdTheme = createTheme();

function DashboardContent() {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(true);
	const toggleDrawer = () => {
		setOpen(!open);
	};
	const selectedReports = useSelector(selectedReportsSelector);
	const selectedDateRange = useSelector(selectedDateRangeSelector);
	const displaySelectedDateRange = useSelector(displaySelectedDateRangeSelector);

	const [openDateRanger, setOpenDateRanger] = React.useState(false);

	const handleDateRangerClickOpen = () => {
		setOpenDateRanger(true);
	};

	const handleDateRangerClose = () => {
		setOpenDateRanger(false);
	};

	const handleDateRangeSave = () => {
		dispatch(updateSelectedDateRange(selectedDateRange));
		setOpenDateRanger(false);
	}

	return (
		<ThemeProvider theme={mdTheme}>
			<Box sx={{ display: 'flex' }}>
				<CssBaseline />
				<AppBar position="absolute" open={open} color='inherit'>
					<Toolbar
						sx={{
							py: '10px'
						}}
					>
						<IconButton
							edge="start"
							color="inherit"
							aria-label="open drawer"
							onClick={toggleDrawer}
							sx={{
								marginRight: '36px',
								...(open && { display: 'none' }),
							}}
						>
							<MenuIcon />
						</IconButton>
						<Grid justify="space-between"
							container
							spacing={4}
						>
							<Grid item sx={{
								flexGrow: 1,
								maxWidth: '400px',
							}}>
								<ClientsAutoComplete />
							</Grid>

							<Grid item sx={{
								flexGrow: 1,
								maxWidth: '450px',
							}}>
								<ReportsAutoComplete />
							</Grid>
							<Grid item item sx={{
								flexGrow: 1,
								maxWidth: '325px',
							}}>
								<TextField
									label="Date Range"
									size='small'
									value={displaySelectedDateRange}
									onClick={handleDateRangerClickOpen}
									placeholder="Date Range"
									sx={{
										width: '375px'
									}}
								/>
								<Dialog open={openDateRanger} onClose={handleDateRangerClose}
									fullWidth={true}
									maxWidth='lg'
								>
									<DialogContent>
										<BasicDateRangePicker />
									</DialogContent>
									<DialogActions>
										<Button onClick={handleDateRangerClose}>Cancel</Button>
										<Button onClick={() => handleDateRangeSave()}>Ok</Button>
									</DialogActions>
								</Dialog>
							</Grid>
							<Grid item >
								<IconButton color="inherit">
									<Badge color="secondary">
										<AccountCircleOutlinedIcon />
									</Badge>
								</IconButton>
							</Grid>
						</Grid>
					</Toolbar>
				</AppBar>
				<Drawer variant="permanent" open={open}>
					<Toolbar
						sx={{
							display: 'flex',
							alignItems: 'center',
							justifyContent: 'flex-end',
							px: [1],
						}}
					>
						<Typography
							edge="start"
							color="inherit"
							component="h1"
							variant="h6"
							color="inherit"
							noWrap
							sx={{ flexGrow: 1 }}
						>
							<img height='50px' width='100px' src='/logo.svg' />
						</Typography>

						<IconButton onClick={toggleDrawer}>
							<ChevronLeftIcon />
						</IconButton>
					</Toolbar>
					<Divider />
					<List>{mainListItems}</List>
				</Drawer>
				<Box
					component="main"
					sx={{
						backgroundColor: (theme) =>
							theme.palette.mode === 'light'
								? theme.palette.grey[100]
								: theme.palette.grey[900],
						flexGrow: 1,
						height: '100vh',
						overflow: 'auto',
					}}
				>
					<Toolbar />
					<Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
						<Grid container spacing={3}>
							<Grid item xs={12}>
								<Paper
									sx={{
										px: 2,
										display: 'flex',
										flexDirection: 'column',
										height: 'auto',
									}}
								>
									<TopCard />
								</Paper>
							</Grid>
							<Grid item
								xs={12}
							>
								<Grid container spacing={3} >
									{
										selectedReports ?
											selectedReports.map((report, index) => {
												return <Grid key={report?.type} item xs={
													(index === selectedReports?.length - 1
														&& selectedReports?.length % 2 === 1
													) ? 12
														:
														selectedReports?.length > 1 ? 6 : 12
												} >
													< ChartWrapper report={report} />
												</Grid>
											})
											: null
									}
								</Grid>
							</Grid>
							<Grid item xs={12}>
								< ChartWrapper report={{ description: 'A Manager Testing Outbounds Calls By SubCase', type: 'Type Result', bottomMost: true }} />
							</Grid>
						</Grid>
					</Container>
				</Box>
			</Box>
		</ThemeProvider >
	);
}

export default function Dashboard() {
	return <DashboardContent />;
}
