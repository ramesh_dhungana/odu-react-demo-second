import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { useDispatch, useSelector } from "react-redux";
import { selectedReportsSelector } from '../slices/home';
import { updateSelectedReports } from '../slices/home'

export default function ReportsAutoComplete() {
	const dispatch = useDispatch();
	let selectedReports = useSelector(selectedReportsSelector);
	const handleMuiSelectOnChange = (event, value) => {
		event.preventDefault();
		dispatch(updateSelectedReports(value));
	}

	return (
		<Autocomplete
			value={selectedReports}
			limitTags={2}
			multiple
			id="tags-reports"
			size='small'
			options={ReportOptions}
			getOptionLabel={(option) => option?.label}
			filterSelectedOptions
			onChange={(event, value) => handleMuiSelectOnChange(event, value)}
			renderInput={(params) => (
				<TextField
					{...params}
					label="Reports"
					placeholder="Reports"
				/>
			)}
		/>
	);
}

const ReportOptions = [
	{ label: 'Calls By Direction', description: 'Multi Clients Calls By', type: 'Direction' },
	{ label: 'Calls By Result', description: 'Multi Clients Calls By', type: 'Result' },
	{ label: 'Calls By Language', description: 'Multi Clients Calls By', type: 'Language' },
	{ label: 'Calls By Retainer', description: 'Multi Clients Calls By', type: 'Retainer' },
	{ label: 'Retainer By Status', description: 'Multi Clients Calls By', type: 'Status' },
];