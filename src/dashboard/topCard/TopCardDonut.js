import { Typography } from '@mui/material';
import React from 'react';
import Chart from 'react-apexcharts'
import { Grid } from '@mui/material'

const TopCardDonut = () => {
	const chartState = {
		options: {
			legend: {
				show: false
			},
		},
		series: [17.8, 82.2],
		labels: ['A', 'B'],
	};

	return (
		<div>
			<Grid container alignItems="center"
				justifyContent="center"
				spacing={1}>
				<Grid item>
					<div className="donut">
						<Chart options={chartState.options} series={chartState.series} type="donut" width="280" />
					</div>
				</Grid>
				<Grid item align='center'>
					<Typography variant='caption' justifyContent="center"
						sx={{
							color: 'gray',
							fontSize: '15px'
						}}>
						Conversion <br/> Rate
					</Typography>
				</Grid>
			</Grid>
		</div>
	);
}

export default TopCardDonut;