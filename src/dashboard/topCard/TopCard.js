import * as React from 'react';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import TopCardDonut from './TopCardDonut';
import TopCardLeft from './TopCardLeft';
import TopCardCenter from './TopCardCenter';

export default function TopCard() {

	return (
		<Grid container>
			<Grid item xs={4}>
				<TopCardLeft />
			</Grid>
			<Divider orientation="vertical" flexItem
				sx={{
					borderRightWidth: 3,
					height: '220px'
				}}>
			</Divider>
			<Grid item xs={4}>
				<TopCardCenter />
			</Grid>
			<Divider orientation="vertical" flexItem
				sx={{
					borderRightWidth: 3,
					height: '220px'
				}}>
			</Divider>
			<Grid item xs={3}>
				<TopCardDonut />
			</Grid>
		</Grid>
	);
}

