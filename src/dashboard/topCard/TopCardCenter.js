import React from 'react';
import { Grid, ListItem, ListItemText, Typography, ListItemIcon } from '@mui/material'
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import { makeStyles } from '@material-ui/core/styles';
import TextSnippetOutlinedIcon from '@mui/icons-material/TextSnippetOutlined';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

const useStyles = makeStyles({
	phoneMessageIcon: {
		width: 600,
		height: 600,
	},
	ulClass: {
		listStyleType: 'none',
		paddingLeft: 0,
		display: 'inline-block',
		textAlign: 'left'
	},
	statsTitle: {
		color: '#2EB9D2',
		fontSize: 16
	},
	statsData: {
		color: 'gray',
		fontSize: 14
	}
});

const TopCardCenter = (props) => {
	const classes = useStyles(props);
	return (
		<div>
			<Grid container spacing={1} justifyContent="center"
				alignItems="center"
			>
				<Grid item xs={5} >
					<Box sx={{ width: '100%', pl: '25px' }}>
						<nav>
							<List>
								<ListItem>
									<ListItemIcon style={{ fontSize: '3000px' }}>
										<TextSnippetOutlinedIcon htmlColor="#D2942E" style={{ fontSize: '75px' }} />
									</ListItemIcon>
								</ListItem>
								<ListItem>
									<ListItemText>
										<Typography variant='h4' color='black' fontWeight='bold' >
											25
										</Typography>
									</ListItemText>
								</ListItem>
								<ListItem>
									<ListItemText>
										<Typography variant='caption' fontSize='15px' color='gray' >
											Retainer <br /> Documents
										</Typography>
									</ListItemText>
								</ListItem>
							</List>
						</nav>
					</Box>
				</Grid>
				<Grid item xs={7}>
					<ul className={classes.ulClass}>
						<li><span className={classes.statsTitle}>Client Name:</span> <span className={classes.statsData} >John Cox Law</span></li>
						<li><span className={classes.statsTitle}>Status:</span> <span className={classes.statsData}>IN_PROGRESS</span></li>
						<li><span className={classes.statsTitle}>Injured Name Direction:</span> <span className={classes.statsData}>Christopher Smith</span></li>
						<li><span className={classes.statsTitle}>Call Start:</span> <span className={classes.statsData}>03-Jan-2021 09:45</span></li>
						<li><span className={classes.statsTitle}>Call End:</span> <span className={classes.statsData}>03-Jan-2021 10:14</span></li>
					</ul>
					<Grid
						container
						direction="row"
						justifyContent="space-around"
						alignItems="flex-start"
					>
						<Grid item xs={4}>
							<PlayArrowIcon style={{ fontSize: '50px' }} />
						</Grid>
						<Grid item xs={4} >
							<div style={{ color: '#557FEF', fontSize: '14px', fontWeight: 'bold', marginTop: '15px' }}>
								View More
							</div>
						</Grid>
						<Grid item xs={4}>
							<ArrowRightAltIcon style={{ color: '#557FEF', fontSize: '30px', marginTop: '10px' }} />
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</div >
	);
}

export default TopCardCenter;
