import * as React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import LiveHelpIcon from '@mui/icons-material/LiveHelp';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import BarChartOutlinedIcon from '@mui/icons-material/BarChartOutlined';
import RecordVoiceOverOutlinedIcon from '@mui/icons-material/RecordVoiceOverOutlined';

const baseMenuStyle = { color: '#557FEF' };
export const mainListItems = (
	<div>
		<ListItem button>
			<ListItemIcon>
				<DashboardIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="DASHBOARD" />
		</ListItem>
		<ListItem button>
			<ListItemIcon>
				<RecordVoiceOverOutlinedIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="RECORDINGS" />
		</ListItem>
		<ListItem button>
			<ListItemIcon>
				<BarChartOutlinedIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="REPORTS" />
		</ListItem>
		<ListItem button>
			<ListItemIcon>
				<PeopleIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="CONTACT US" />
		</ListItem>
		<ListItem button>
			<ListItemIcon>
				<LiveHelpIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="FAQ" />
		</ListItem>
		<ListItem button>
			<ListItemIcon>
				<SettingsOutlinedIcon style={baseMenuStyle} />
			</ListItemIcon>
			<ListItemText primary="CHANGE PASSWORD" />
		</ListItem>
	</div>
);
