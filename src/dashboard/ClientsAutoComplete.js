import * as React from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

export default function ClientAutoComplete() {
	return (
		<Autocomplete
			multiple
			limitTags={2}
			id="tags-clients"
			size='small'
			options={ClientOptions}
			getOptionLabel={(option) => option.label}
			filterSelectedOptions
			renderInput={(params) => (
				<TextField
					{...params}
					label="Clients"
					placeholder="Clients"
				/>
			)}
		/>
	);
}

const ClientOptions = [
	{ label: 'Client One', year: 1994 },
	{ label: 'Client Two', year: 1972 },
	{ label: 'Client Three', year: 1974 },
	{ label: 'Client Four', year: 2008 },
	{ label: 'Client Five', year: 1957 },
];