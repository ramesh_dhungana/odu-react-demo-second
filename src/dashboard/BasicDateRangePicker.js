import React, { useEffect, useState } from 'react';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

import { DateRangePicker } from 'react-date-range';
import { useDispatch, useSelector } from 'react-redux';
import { selectedDateRangeSelector } from '../slices/home';
import { updateSelectedDateRange } from '../slices/home';

export default function DateRangerPicker() {
	const dispatch = useDispatch();
	const selectedDateRange = useSelector(selectedDateRangeSelector);
	const [state, setState] = useState(selectedDateRange);

	useEffect(() => {
		dispatch(updateSelectedDateRange(state));
	}, [state])

	return (
		<DateRangePicker
			onChange={item => setState([item.selection])}
			showSelectionPreview={true}
			moveRangeOnFirstSelection={false}
			months={2}
			ranges={state}
			direction="horizontal"
		/>
	)
}