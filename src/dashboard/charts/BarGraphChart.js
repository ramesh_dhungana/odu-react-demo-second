import * as React from 'react';
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

const BarGraphChart = (props) => {
	const state = {

		series: [{
			name: 'Inbounds',
			type: 'column',
			data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6]
		}, {
			name: 'Outbounds',
			type: 'column',
			data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5]
		}],
		options: {
			chart: {
				height: 350,
				type: 'line',
				stacked: false
			},
			legend: {
				show: false
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				width: [1, 1, 4]
			},
			xaxis: {
				categories: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016],
			},
			yaxis: [
				{
					axisTicks: {
						show: true,
					},
					axisBorder: {
						show: true,
						color: '#008FFB'
					},
					labels: {
						style: {
							colors: '#008FFB',
						}
					},
					tooltip: {
						enabled: true
					}
				},
				{
					seriesName: 'Inbounds',
					opposite: true,
					axisTicks: {
						show: true,
					},
					axisBorder: {
						show: true,
						color: '#00E396'
					},
					labels: {
						style: {
							colors: '#00E396',
						}
					},
				},
			],
			tooltip: {
				fixed: {
					enabled: true,
					position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
					offsetY: 30,
					offsetX: 60
				},
			},
			legend: {
				horizontalAlign: 'left',
				offsetX: 40
			}
		},


	};

	return (
		<div id="BarGraphChart">
			<ReactApexChart options={state.options} series={state.series} type="line" height={350} />
		</div>
	);
}
export default BarGraphChart;