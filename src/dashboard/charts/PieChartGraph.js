import React from 'react';
import { Grid } from '@mui/material'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

const PieChartGraph = (props) => {
	const chartState = {
		options: {
			legend: {
				show: true
			},
			labels: ['Outbounds', 'Inbounds'],
		},
		series: [17.8, 82.2],
		labels: ['Outbounds', 'Inbounds'],
	};

	return (
		<div>
			<Grid container alignItems="center"
				justifyContent="center"
				spacing={1}>
				<Grid item>
					<div className="donut">
						<ReactApexChart options={chartState.options} series={chartState.series} type="donut" width="495" />
					</div>
				</Grid>
			</Grid>
		</div>
	);
}

export default PieChartGraph;