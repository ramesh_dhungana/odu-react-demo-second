import React, { useState } from 'react';
import Grid from '@mui/material/Grid';
import CancelIcon from '@mui/icons-material/Cancel';
import BarChartIcon from '@mui/icons-material/BarChart';
import TimelineIcon from '@mui/icons-material/Timeline';
import DonutLargeIcon from '@mui/icons-material/DonutLarge';
import GraphicEqIcon from '@mui/icons-material/GraphicEq';
import TableViewIcon from '@mui/icons-material/TableView';
import BarGraphChart from './BarGraphChart';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import LineGraphChart from './LineGraphChart';
import PieChartGraph from './PieChartGraph';
import AreaGraph from './AreaGraph';
import TableGraph from './TableGraph'
import { useDispatch, useSelector } from "react-redux";
import Paper from '@mui/material/Paper';
import { updateSelectedReports } from '../../slices/home';
import TimeLineGraph from './TimeLineGraph';
import { Typography } from '@mui/material';
import { selectedReportsSelector, displaySelectedDateRangeSelector } from '../../slices/home';

const baseStyle = {
	color: 'gray', marginRight: '5px', cursor: 'pointer',
}
const iconHoverColor = '#000080';

const GraphTypes = {
	BarGraph: 'BarGraph',
	LineGraph: 'LineGraph',
	PieChart: 'PieChart',
	AreaGraph: 'AreaGraph',
	ViewTable: 'ViewTable'
}

export default function ChatWrapper(props) {
	const dispatch = useDispatch();
	const [selectedGraph, setSelectedGraph] = useState(GraphTypes.BarGraph);
	const { report } = props;
	const selectedReports = useSelector(selectedReportsSelector);
	const displaySelectedDateRange = useSelector(displaySelectedDateRangeSelector)

	const onchangeSelectedGraph = (event, type) => {
		event.preventDefault();
		setSelectedGraph(type);
	}

	const removeSelectedGrid = (report) => {
		const filteredReports = selectedReports.filter(item => item.type !== report?.type);
		dispatch(updateSelectedReports(filteredReports));
	}

	const removeBottomMost = (event) => {
	}

	return (
		<div>
			<Paper
				sx={{
					mb: 4,
				}}
			>
				<Grid container spacing={1}>
					<Grid item xs={11}>
					</Grid>
					<Grid item xs={1}>
						{!report?.bottomMost ?
							<Tooltip title="Close">
								<IconButton onClick={(event) => report?.bottomMost ? removeBottomMost(event) : removeSelectedGrid(report)}>
									<CancelIcon style={{ color: 'red', cursor: 'pointer' }}>
									</CancelIcon>
								</IconButton>
							</Tooltip> :
							null}
					</Grid>
					<Grid item xs={6}>
					</Grid>
					<Grid item xs={6} style={{ paddingBottom: '15px' }}>
						<Tooltip title="Bar Graph">
							<IconButton
								style={selectedGraph === GraphTypes.BarGraph ? { backgroundColor: iconHoverColor } : { backgroundColor: 'transparent' }}
								onClick={(event) => onchangeSelectedGraph(event, GraphTypes.BarGraph)} >
								<BarChartIcon style={baseStyle}>
								</BarChartIcon>
							</IconButton>
						</Tooltip>

						<Tooltip title="Line Graph">
							<IconButton
								style={selectedGraph === GraphTypes.LineGraph ? { backgroundColor: iconHoverColor } : { backgroundColor: 'transparent' }}
								onClick={(event) => onchangeSelectedGraph(event, GraphTypes.LineGraph)}>
								<TimelineIcon style={baseStyle}>
								</TimelineIcon>
							</IconButton>
						</Tooltip>

						<Tooltip title="Pie Chart">
							<IconButton
								style={selectedGraph === GraphTypes.PieChart ? { backgroundColor: iconHoverColor } : { backgroundColor: 'transparent' }}
								onClick={(event) => onchangeSelectedGraph(event, GraphTypes.PieChart)}>
								<DonutLargeIcon style={baseStyle}>
								</DonutLargeIcon>
							</IconButton>
						</Tooltip>

						<Tooltip title="Area Graph">
							<IconButton
								style={selectedGraph === GraphTypes.AreaGraph ? { backgroundColor: iconHoverColor } : { backgroundColor: 'transparent' }}
								onClick={(event) => onchangeSelectedGraph(event, GraphTypes.AreaGraph)}>
								<GraphicEqIcon style={baseStyle}>
								</GraphicEqIcon>
							</IconButton>
						</Tooltip>

						<Tooltip title="View Table">
							<IconButton
								style={selectedGraph === GraphTypes.ViewTable ? { backgroundColor: iconHoverColor } : { backgroundColor: 'transparent' }}
								onClick={(event) => onchangeSelectedGraph(event, GraphTypes.ViewTable)}>
								<TableViewIcon style={baseStyle}>
								</TableViewIcon>
							</IconButton>
						</Tooltip>
					</Grid>
					<Grid item xs={12} alignItems={'center'}>
						<Typography fontSize='14px' color='#5B5755' fontWeight='800' fontFamily='Helvetica, Arial, sans-serif' >
							{`${props?.report?.description} ${props?.report?.type} [${displaySelectedDateRange}]`}
						</Typography>
						<Typography fontSize='16px' color='#5B5755' fontFamily='Helvetica, Arial, sans-serif' paddingBottom='15px'>
							Total calls, groups by inbound/outboud origination
						</Typography>
					</Grid>
					{selectedGraph === GraphTypes.BarGraph ?
						<Grid item xs={12}>
							{report?.bottomMost ?
								<TimeLineGraph report={report} />
								:
								<BarGraphChart report={report} />
							}
						</Grid> : null
					}
					{selectedGraph === GraphTypes.LineGraph ?
						<Grid item xs={12}>
							<LineGraphChart report={report} />
						</Grid> : null
					}
					{selectedGraph === GraphTypes.PieChart ?
						<Grid item xs={12}>
							<PieChartGraph report={report} />
						</Grid> : null
					}
					{selectedGraph === GraphTypes.AreaGraph ?
						<Grid item xs={12}>
							<AreaGraph report={report} />
						</Grid> : null
					}
					{selectedGraph === GraphTypes.ViewTable ?
						<Grid item xs={12}>
							<TableGraph report={report} />
						</Grid> : null
					}
				</Grid>
			</Paper>
		</div >
	);
}

