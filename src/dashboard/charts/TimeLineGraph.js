
import React from 'react';
import moment from 'moment';
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';
import { displaySelectedDateRangeSelector } from '../../slices/home';

const TimeLineGraph = (props) => {
	const displaySelectedDateRange = useSelector(displaySelectedDateRangeSelector);

	const state = {
		series: [
			{
				data: [
					{
						x: 'Admins',
						y: [
							new Date('2019-03-04').getTime(),
							new Date().getTime()
						],
						fillColor: '#E32450'
					},
					{
						x: 'Intakes',
						y: [
							new Date('2019-02-27').getTime(),
							new Date().getTime()
						],
						fillColor: '#DC7100'
					},
				]
			}
		],
		options: {
			chart: {
				height: 350,
				type: 'rangeBar'
			},
			plotOptions: {
				bar: {
					horizontal: true,
					distributed: true,
					dataLabels: {
						hideOverflowingLabels: false
					}
				}
			},
			dataLabels: {
				enabled: true,
				formatter: function (val, opts) {
					var label = opts.w.globals.labels[opts.dataPointIndex]
					var a = moment(val[0])
					var b = moment(val[1])
					var diff = b.diff(a, 'days')
					return label + ': ' + diff + (diff > 1 ? ' days' : ' day')
				},
				style: {
					colors: ['#f3f4f5', '#fff']
				}
			},
			xaxis: {
				type: 'datetime'
			},
			yaxis: {
				show: false
			},
			grid: {
				row: {
					colors: ['#f3f4f5', '#fff'],
					opacity: 1
				}
			}
		},


	};

	return (
		<div id="chart">
			<ReactApexChart options={state.options} series={state.series} type="rangeBar" height={350} />
		</div>
	);
}

export default TimeLineGraph;