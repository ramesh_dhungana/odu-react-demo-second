import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
	[`&.${tableCellClasses.head}`]: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	[`&.${tableCellClasses.body}`]: {
		fontSize: 14,
	},
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
	'&:nth-of-type(odd)': {
		backgroundColor: theme.palette.action.hover,
	},
	// hide last border
	'&:last-child td, &:last-child th': {
		border: 0,
	},
}));

function createData(category, outbound, inbound) {
	return { category, outbound, inbound };
}

const rows = [
	createData('12/27/2021', 159, 24),
	createData('12/28/2021', 237, 37),
	createData('12/29/2021', 262, 24),
	createData('12/30/2021', 305, 67),
	createData('12/31/2021', 356, 49),
];

export default function TableGraph(props) {
	return (
		<div component={Paper}>
			<TableContainer >
				<Table sx={{ minWidth: 500 }} aria-label="customized table">
					<TableHead color='primary'>
						<TableRow>
							<StyledTableCell align="center">Category</StyledTableCell>
							<StyledTableCell align="center">Outbounds</StyledTableCell>
							<StyledTableCell align="center">Inbounds</StyledTableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{rows.map((row) => (
							<StyledTableRow key={row.category}>
								<StyledTableCell align="center">{row.category}</StyledTableCell>
								<StyledTableCell align="center">{row.outbound}</StyledTableCell>
								<StyledTableCell align="center">{row.inbound}</StyledTableCell>
							</StyledTableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</div >
	);
}
